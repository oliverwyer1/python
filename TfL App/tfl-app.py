# import requests library - done by running - pip install requests
import requests
import json
import datetime

# app key and ID
# VALUES LEFT BLANK IN COMMIT - USED IN PRIVATE SESSIONS
appId = ''
appKey = ''

# new function for checking the tube
def tubeCheck():
    getRequest = requests.get(f"https://api.tfl.gov.uk/line/mode/tube/status")
    print(f"Status code from GET is {getRequest.status_code}")
    print(f"The current type of getRequest variable is {type(getRequest)}")

    print("""Welcome to the TfL Underground checker!
Please enter a number for the line you want to check!
0 - Bakerloo
1 - central
2 - circle
3 - district
4 - hammersmith & City
5 - jubilee
6 - metropolitan
7 - northern
8 - piccadilly
9 - victoria
10 - waterloo & city
    """)
    try:
        # getting the input as an integer
        number = int(input(">"))
        print(f"You have selected {number}")
        rawData = getRequest.json()
        tubeLine = rawData[number]
        #print(f"The type of the tubeLine variable is {type(tubeLine)}")
        print(f"Welcome to the {tubeLine['name']} line!")
        # # accessing an element in a nested dictionary
        print(f"The current status on the {tubeLine['name']} line is {tubeLine['lineStatuses'][0]['statusSeverityDescription']}")
        print(f"\nNow showing all available stops for the {tubeLine['name']} line:")
        getRequest = requests.get(f"https://api.tfl.gov.uk/line/{tubeLine['name']}/stoppoints")
        stopPoints = getRequest.json()
        stopCount = 0
        for stop in stopPoints:
            print(stopPoints[stopCount]['commonName'])
            stopCount += 1
        enter = input("\nPress enter to return to the menu!")
    # handling errors if user enters anything else other than a number or an invalid number
    except UnboundLocalError as e:
        print("Error! Please enter a number!")
        print("UnboundLocalError")
        print(e)
    except ValueError as e:
        print("Error! Please enter a number")
        print("ValueError")
        print(e)
    except IndexError as e:
        print("Error! Please enter a valid number!")
        print("IndexError")
        print(e)

# new function for checking the roads
def roadCheck():
    # error handling
    try:
        getRequest = requests.get(f"https://api.tfl.gov.uk/road")
        print(f"Status code from GET is {getRequest.status_code}")
        print(f"The current type of getRequest variable is {type(getRequest)}")
        rawData = getRequest.json()
        print("""Welcome to TfL Road checker!
Type in the number that you would like to check!
0 - A1
1 - A10
2 - A12
3 - A13
4 - A2
5 - A20
6 - A205
7 - A21
8 - A23
9 - A24
10 - A3
11 - A316
12 - A4
13 - A40
14 - A406
15 - A41
16 - Bishopsgate Cross Route
17 - Blackwall Tunnel
18 - City Route
19 - Farringdon Cross Route
20 - Inner Ring
21 - Southern River Route
22 - Western Cross Route
        """)
        roadName = int(input(">"))
        roadData = rawData[roadName]
        print(f"Now viewing data for {roadData['displayName']}")
        print(f"The current status of {roadData['displayName']} is {roadData['statusSeverity']} with {roadData['statusSeverityDescription']}")
        enter = input("Press enter to return to the main menu!")
        # handling errors if user enters anything else other than a number or an invalid number
    except UnboundLocalError as e:
        print("Error! Please enter a number!")
        print("UnboundLocalError")
        print(e)
    except ValueError as e:
        print("Error! Please enter a number")
        print("ValueError")
        print(e)
    except IndexError as e:
        print("Error! Please enter a valid number!")
        print("IndexError")
        print(e)

# new function for planning a route
def travelRoute():
    # try/except block for error handling
    try:
        # start
        print("""Please enter the name of the underground station where you are travelling from (i.e. Euston):
Please note, travel information will be based on the current time.""")
        startingLong = input(">")
        print(f"You have selected {startingLong}")
        # split the string into a list and rejoin it into a single word - used for the get request
        #startingLong = startingLong.split(' ')
        #startingString = ''.join(startingLong)
        #print(startingLong)
        # end
        print("Please enter the name of the underground station - end destination (i.e. Westminster):")
        endLong = input(">")
        print(f"You have selected {endLong}")
        # split the string into a list and rejoin it into a single word - used for the get request
        #endLong = endLong.split(' ')
        #endString = ''.join(endLong)
        #print(endLong)
        print("Now fetching route, please wait...")
        # find the icsCode for the start and end destination - used to build another GET request
        # GET request
        getRequest = requests.get(f"https://api.tfl.gov.uk/journey/journeyresults/{startingLong}/to/{endLong}&app_id={appId}&app_key={appKey}")
        #print(f"Status code from GET is {getRequest.status_code}")
        rawData = getRequest.json()
        # finding icsCode for starting point
        startPoint = rawData['fromLocationDisambiguation']['disambiguationOptions'][0]['parameterValue']
        #print(type(startPoint))
        print(f"The icsCode for the starting point is {startPoint}")
        # finding icsCode for end point
        endPoint = rawData['toLocationDisambiguation']['disambiguationOptions'][0]['parameterValue']
        #print(type(endPoint))
        print(f"The icsCode for the end point is {endPoint}")
        # building new GET request
        rawJourneyData = requests.get(f"https://api.tfl.gov.uk/journey/journeyresults/{startPoint}/to/{endPoint}&app_id={appId}&app_key={appKey}")
        #print(f"Status code from GET is {rawJourneyData.status_code}")
        fullRouteResponse = rawJourneyData.json()
        journeyTime = fullRouteResponse['journeys'][0]['duration']
        print(f"The overall journey time will be {journeyTime} minutes")
        print("The journey steps are:\n")
        selectedRoute = fullRouteResponse['journeys'][0]['legs']
        for detail in selectedRoute:
            print("From", detail['departurePoint']['commonName'])
            print(detail['instruction']['detailed'])
            print("Arrive at", detail['arrivalPoint']['commonName'])
            # displaying disruptions (if there are any)
            isDisrupted = detail['isDisrupted']
            if isDisrupted == True:
                for disruption in detail['disruptions']:
                    print("\n=======Disruption:=======")
                    print(disruption['description'])
            else:
                print("No disruptions reported.\n")
            print("\n")
    # handling KeyError specifically - found this in testing
    except KeyError as e:
        print("Error (KeyError)! Please try again!")
    # handling TypeError
    except TypeError as e:
        print("Error (TypeError)! Please try again!")
    print("Press enter to return to the main menu.")
    enter = input("")

valid = True
while valid == True:
    print("""Welcome to the TfL checker!
Please specify which service you would like to check!
1 - Underground
2 - Roads
3 - Route Checker
Type 'q' to quit.""")
    check = input(">")
    if check == '1':
        tubeCheck()
    elif check == '2':
        roadCheck()
    elif check == '3':
        travelRoute()
    elif check == 'q':
        print("Goodbye!")
        valid = False
    else:
        print("Error! Please try again!")