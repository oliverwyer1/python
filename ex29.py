# new variables defined
people = 20
cats = 30
dogs = 15

# if statement, if the condition is true, do the following!
# Python relies on indentation to define scope in the code
# if there is no indented block, it throws an error
if people < cats & cats > dogs:
    print("Too many cats! The world is doomed!")

if people > cats:
    print("Not many cats! The world is saved!")

if people < dogs:
    print("The world is drooled on!")

if people > dogs:
    print("The world is dry!")

# incrementing the value of dogs by 5
dogs += 5

if people >= dogs:
    print("People are greater than or equal to dogs.")

if people <= dogs:
    print("People are less than or equal to dogs.")

if people == dogs:
    print("People are dogs.")