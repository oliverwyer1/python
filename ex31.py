print("""
You enter a dark room with two doors.
Do you go through #1 or door #2?
""")

door = input("> ")

if door == "1":
    print("There's a giant bear here eating a cheesecake.")
    print("What do you do?")
    print("1. Take the cake.")
    print("2. Scream at the bear.")

    bear = input("> ")

    if bear == "1":
        print("The bear eats the cake!")
    elif bear == "2":
        print("The bear screams at you!")
    else:
        print(f"Well, doing {bear} is probably better.")
        print("The bear runs away!")
elif door == 2:
    print("You stare into an endless abyss!")
    print("1. Blueberries.")
    print("2. Yellow jacket clothespins.")
    print("3. Understanding revolvers yelling melodies.")

    insanity = input("> ")

    if insanity == "1" or insanity == "2":
        print("Your body survives powered by a mind of jello.")
        print("Good job!")
    else:
        print("You close the door and turn around.")
        print("Good job!")

else:
    print("You turn around and leave the two doors alone. Good job!")