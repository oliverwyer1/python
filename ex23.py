# this exercise is going to demonstrate the following:
# how computers store human languages for display and processing and how python 3 calls this strings
# how you need to "encode" and "decode" Python's strings into a type called bytes
# how to handle errors in your string and byte handling
# how to read code and finding out what it means if you've never seen it before

# plus if-statement and lists for processing a list of things

# import sys module
import sys

# three arguments for argv
script, input_encoding, error = sys.argv


# new method defined with three arguments
def main(language_file, encoding, errors):
    # readline reads a line from the input file
    line = language_file.readline()

    # if line returns true (if it has something in it), run the following
    if line:
        print_line(line, encoding, errors)
        return main(language_file, encoding, errors)


# new method with three arguments
def print_line(line, encoding, errors):
    # strip() = Return a copy of the string with leading and trailing whitespace removed. If chars is given and not None, remove characters in chars instead.
    next_lang = line.strip()
    # encode() = encode the string using the codec registered for encoding
    #raw_bytes 
    raw_bytes = next_lang.encode(encoding, errors=errors)
    #decode() = decode the bytes using the codec registered for encoding
    cooked_string = raw_bytes.decode(encoding, errors=errors)

    print(raw_bytes, "<===>", cooked_string)

#the codec being used here is UTF-8
#the file being used is languages.txt
languages = open("languages.txt", encoding="utf-8")

# call the method main with three arguments
main(languages, input_encoding, error)

# in the output, we can see the bytes are inside the 'b'
# which is then converted to UTF-8 (for example)
# on the left is the number for each byte and the right has the character output
# the left is the Python numerical bytes (or the bytes that Python uses to store the string)
# on the right is where you can see the real characters in your terminal.


# if you have raw bytes, you must decode to get the string
# if you have a string, you must encode to get the raw bytes