# new variables
people = 30
cars = 40
trucks = 15

if cars > people:
    print("We should take the cars.")
# else if statement
elif cars < people:
    print("We should not take the cars.")
else:
    print("We can't decide.")

# new if statement
if trucks > cars:
    print("That's too many trucks!")
#this is another 'branch' - if the if statement returns false, then this one will be checked if it's true
elif trucks < cars:
    print("Maybe we could take the trucks.")
# if the the first if and elif are false, return else
else:
    print("We still can't decide.")

#new if statement
if trucks > cars:
    print("That's too many trucks.")
elif trucks < cars:
    print("Maybe we could take the trucks.")
else:
    print("We still can't decide")

if people > trucks:
    print("Alright, let's just take the trucks.")
else:
    print("Find, let's stay home then.")