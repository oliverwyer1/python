# standard deviation - a number that describes how spread out the values are.
# a low standard deviation means that most of the numbers are close to the mean (average) value
# a high standard deviation means that the values are spread out over a wider range

import numpy

range1 = [55, 56, 54, 57, 55, 54, 56]
range2 = [100, 23, 55, 17, 63, 32, 145]

stanDevLow = numpy.std(range1)
stanDevHigh = numpy.std(range2)

# displays value to 2 decimal places
# displays 1.03 - a low standard deviation, the numbers in range1 are close to the mean!
print(round(stanDevLow, 2))
# displays 42.81 - a high standard deviation, the values are spread out over a wide range!
print(round(stanDevHigh, 2))

# Variance - a number that indicates how spread out the values are
# if you take the square root of the variance - you get the standard deviation
# if you multiply the standard deviation by itself - you get the variance

# how to find the variance:
# find the mean
# for each value, find the difference from the mean - i.e. value - mean = difference
# for each difference, find the square value (difference)2 = square value
# the variance is the average of the squared differences

range3 = [33, 63, 10, 94, 84, 37, 45, 100, 67]

variance = numpy.var(range3)

print(round(variance, 2))