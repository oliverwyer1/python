#we're using the letter 'f' for format and {} for referring to variables
#'f' tells Python the format the string and put these variables in

#new variables defined
name = 'Joshua Blewitt'
age = 27 #really
height = 188 #in cm - I'm 6'2
weight = 89 #in kg
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

#print the variables
print(f"Let's talk about {name}.")
print(f"He's {height} cm tall.")
print(f"He's {weight} kg heavy.")
print("Actually, that's not too heavy.")
print(f"He's got {eyes} eyes and {hair} hair")
print(f"His teeth are {teeth}.")

#new variable where the value is the sum of multiple variables
total = age + height + weight
print(f"If I add {age}, {height}, and {weight} I get {total}")

#calculating cm to inches
height_inches = weight * 0.39
print(f"{height} cm in inches is {height_inches}.")

#calculating kg to pounds
weight_pounds = weight * 2.2046
print(f"{weight} kg in pounds is {weight_pounds}")