#new functions
def add(a, b):
    print(f"ADDING {a} + {b}")
    return a + b

def subtract(a, b):
    print(f"SUBTRACTING {a} - {b}")
    return a - b

def multiply(a, b):
    print(f"MULTIPLYING {a} * {b}")
    return a * b

def divide(a, b):
    print(f"DIVIDING {a} / {b}")
    return a / b

print("Let's do some math with just functions!")

#this will be 27
age = add(22, 5)

#this will be 188
height = subtract(192, 4)

#this will be 88
weight = multiply(44, 2)

#this will be 50
iq = divide(100, 2)

print(f"Age: {age}, Height: {height}, Weight: {weight}, IQ: {iq}")


#first, this takes the divide function by taking the IQ value and dividing it by 2 - giving 25
#next, multiply weight (88) and 25.0
#next, subtract height (188) and 2200.0 - this gives -2012
#finally, add 27 to -2012.0, resulting in -1985.0
what = add(age, subtract(height, multiply(weight, divide(iq, 2))))

print(f"That becomes: {what}, can you do it by hand?")