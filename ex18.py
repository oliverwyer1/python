#functions do three things
#they name pieces of code the way variables name strings and numbers
#they take arguments the way your scripts take argv
#using 1 and 2, they let you make your own "mini-scripts" or "tiny commands"

#functions are defined by def
#args is a tuple!
#the * tells python to take all the arguments to the function and then put them in args as a list
def print_two(*args):
    arg1, arg2 = args
    print(f"arg1: {arg1}, arg2: {arg2}")

#so that *args is pointless, we can just do this
def print_two_again(arg1, arg2):
    print(f"arg1: {arg1}, arg2: {arg2}")

#this just takes a single argument
def print_one(arg1):
    print(f"arg1: {arg1}")

#this one takes no arguments
def print_none():
    print("I got nothin'.")

#lets call the functions!
print_two("Joshua","Blewitt")
print_two_again("Joshua","Blewitt")
print_one("One!")
print_none()