#for input, you can specifiy the prompt to show the user what to type
#we can put a string inside the ()
age = input("How old are you? ")
height = input("How tall are you? ")
weight = input("How much do you weigh? ")

print(f"So, you're {age} old, {height} tall and {weight} heavy.")