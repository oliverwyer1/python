#import modules
from sys import argv
from os.path import exists

#three arguments needed when the script is ran in the terminal
script, from_file, to_file = argv

print(f"Copying from {from_file} to {to_file}")

#open the file and assign contents of file to new variable
in_file = open(from_file)
indata = in_file.read()

#print the size of the file
print(f"The input file is {len(indata)} bytes long")

#return true or false if output file exists
print(f"Does the output file exist? {exists(to_file)}")
print("Ready, hit RETURN to continue, CTRL-C to abort.")
input()

#remember to use 'w' for mode, that's write
out_file = open(to_file,'w')
#copy the data to the new file
out_file.write(indata)

print("Alright, all done.")

#close the files
out_file.close()
in_file.close()